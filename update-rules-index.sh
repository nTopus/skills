#!/bin/bash -e

cd rules

(
echo 'export default ['
for rule in *.md; do
  echo "  '$rule',"
done
echo ']'
) > index.js
