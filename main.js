import ruleFiles from './rules/index.js'

const rules = {}, skills = {}

const mainDir = import.meta.url.replace(/\/[^\/]*$/, '')
const rulesDir = mainDir+'/rules'

console.log(rulesDir)

Promise.all(
  ruleFiles.map((file, i)=> {
    file = file.replace(/\.md$/, '')
    return fetch(rulesDir+'/'+file+'.md')
    .then(res => res.text())
    .then(code => compile(file, code))
    .then(res => rules[file] = res)
  })
)
.then(async ()=> {
  document.getElementById('content').innerHTML =
  Object.entries(rules)
  .filter(([file])=> !file.match(/(^|\/)@/))
  .sort((a,b)=> a[0]<b[0] ? -1 : 1 )
  .map(([file, rule])=> `
    <div class="rule">
      <article>
        <h3>${rule.title}</h3>
        <div>${rule.body}</div>
      </article>
      <div class="skills">
      ${formatSkills(rule.fullSkills)}
      </div>
    </div>
  `)
  .join('')
})
.catch(err => {
  console.error('Fail to init.', err)
  alert(err.message)
})

async function compile(file, code) {
  code = code.replace(/\r/g, '').split('\n')
  const title = code.shift().replace(/^\s*#/, '').trim()
  const blocks = code.join('\n').split(/\n\s*\n/)
  const defsCode = blocks.pop()
  const body = blocks.join('\n').replace(/^\s*\n/, '')
  console.log('BLOCKS:', file, { title, body, defsCode })
  const rootIndent = defsCode.split('\n')[0].replace(/^(\s+).*/, '$1')
  const defs = { file }
  defsCode.replace(new RegExp(`(^|[\n])${rootIndent}\\*\\s*`, 'g'), '\0')
      .split('\0')
      .forEach(def => {
        if (def.split(':')[0]) {
          console.log('===>', file, def.split(':'))
          defs[camelize(def.split(':')[0])] = def.replace(/^[^:]+:[ \t]*\n?/, '')
        }
      })
  await parseExtends(defs)
  await parseFullSkills(defs)
  return {
    ...defs,
    title,
    body,
    defs
  }
}

async function parseExtends(defs) {
  defs.extends = defs.extends?.split(',').map(f => f.trim()) || []
}

async function parseFullSkills(defs) {
  const lvlSkills = defs['levelSkills'] ? parseSkills(defs['levelSkills']) : {}
  defs.skills = defs.skills ? parseSkills(defs.skills) : []
  for (let file of defs.extends) await waitForCompiled(file)
  const deps = defs.extends.map(f => rules[f].skills)
  defs.fullSkills = joinSkills(...deps, defs.skills, lvlSkills)
}

function parseSkills(skillMDList) {
  skillMDList = skillMDList.trimRight().split('\n')
  const skills = { __key: '__ROOT__' }
  let curSkillNest
  let lastSkill
  const indentStack = []
  let curIndent = -1

  function getIndent(line) {
    return line.replace(/^(\s+).*/, '$1').length
  }

  function up(lineIndent) {
    console.log('UP', curIndent, lineIndent, lastSkill?.__key)
    curIndent = lineIndent
    indentStack.push(curIndent)
    if (!lastSkill) curSkillNest = skills
    else curSkillNest = lastSkill.__sub = { __parent: curSkillNest }
  }

  function down(lineIndent) {
    console.log('DOWN', curIndent, lineIndent)
    const origIndent = curIndent
    while (curIndent !== lineIndent) {
      if (indentStack.length === 0) throw Error(`Skill indent fail! From ${origIndent} to ${lineIndent}.`)
      indentStack.pop()
      curIndent = indentStack[indentStack.length-1]
      curSkillNest = curSkillNest.__parent
      console.log('DOWN to', curIndent, curSkillNest?.__key)
    }
  }

  for (let line of skillMDList) {
    let lineIndent = getIndent(line)
    if (lineIndent > curIndent) up(lineIndent)
    if (lineIndent < curIndent) down(lineIndent)
    let __key = line.split(':')[0].replace(/^[\s*]*|\s*$/g, '')
    let __level = (line.split(':')[1] || '').trim()
    lastSkill = curSkillNest[__key] = { __key, __level }
  }
  return skills
}

function waitForCompiled(file) {
  console.log(`Waiting for "${file}"...`)
  return new Promise((resolve, reject)=> {
    const started = Date.now()
    const tryer = ()=> {
      if (rules[file]?.fullSkills) return resolve(rules[file])
      if ((Date.now()-started) > 5000) return reject(Error(`Timeout trying to load "${file}".`))
      setTimeout(tryer, 50)
    }
    tryer()
  })
}

function joinSkills(...trees) {
  trees = trees.filter(t => !!t)
  console.log('TREES', trees)
  let joined = {}
  trees.forEach(tree => {
    joined = { ...joined, ...tree }
  })
  Object.keys(joined)
  .filter(skill => !skill.match(/^__/))
  .forEach(skill => {
    joined[skill].__sub = joinSkills(...trees.map(t => t[skill]?.__sub))
  })
  delete joined.__parent
  return joined
}

function formatSkills(skills) {
  if (!skills) return ''
  skills = Object.entries(skills).filter(([skill])=> !skill.match(/^__/))
  if (skills.length === 0) return ''
  return '<ul>' +
    skills
    .sort(([a], [b])=> a<b ? -1 : 1)
    .map(([skill, data])=>
      '<li><div class="skill__head">' +
      `<div class="skill__name">${skill}</div>` +
      `<span class="${lvlClass(data.__level)}">${data.__level}</span></div>` +
      formatSkills(data.__sub) +
      '</li>'
    ).join('') +
    '</ul>'
}

function lvlClass(lvl){
  lvl = lvl.toLowerCase().replace(/[^a-z]/g, 'x')
  return `skill__level skill__level--${lvl||'empty'}`
}

function camelize(str) {
  return str.replace(/[-_ ]+(.)/g, (_, char)=> char.toUpperCase())
}
