# Desenvolvedor Junior
  * extends: @dev-lvl1
  * skills:
    * estrutura-de-dados
      * fila: avançado
      * pilha: avançado
      * árvore: intermediário
      * lista-encadeada: básico
      * lista-circular: básico
      * testes
        * TDD: intermediário
        * BDD: básico
    * comunicação
      * documentar: intermediário
