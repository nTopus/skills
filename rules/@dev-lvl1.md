# Desenvolvedor Trainee
  * extends: @dev-lvl0
  * level skills:
    * assiduidade-formação
  * skills:
    * estrutura-de-dados
      * fila: intermediário
      * pilha: intermediário
      * árvore: básico
    * testes
      * TDD: básico
    * comunicação
      * apresentar: básico
      * documentar: básico
