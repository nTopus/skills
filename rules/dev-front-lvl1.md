# Desenvolvedor Front-end Trainee

Um estudante torando-se um profissional.

  * extends: @dev-lvl1, dev-front-lvl0
  * skills:
    * web-dev: básico
      * Javascipt: básico
        * JS-OO: básico
      * Typescript: básico
      * CSS: básico
    * http: básico
    * testes
      * cypress: básico
