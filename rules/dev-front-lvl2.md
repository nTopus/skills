# Desenvolvedor Front-end Junior

Um profissional jovem, pedalando se rodinhas.

  * extends: @dev-lvl2, dev-front-lvl1
  * skills:
    * web-dev: intermediário
      * Javascipt: intermediário
        * JS-OO: intermediário
        * JS-Funcional: básico
      * DOM: intermediário
      * HTML: intermediário
      * CSS: intermediário
    * http: intermediário
    * testes
      * cypress: intermediário
      * tdd-componentes-headless: básico
        * lib-jsdom: básico
